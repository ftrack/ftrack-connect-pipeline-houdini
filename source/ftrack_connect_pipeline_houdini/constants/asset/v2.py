# :coding: utf-8
# :copyright: Copyright (c) 2014-2021 ftrack

from ftrack_connect_pipeline.constants.asset.v2 import *

ASSET_LINK = 'asset_link'

# TODO: In case we want to add specialized keys to the keys list
# KEYS.append('keyToAppend')